/*
 * mcu_temp.h
 *
 * Created: 08-04-2018 11:17:18 PM
 *  Author: abhra
 */ 


#ifndef MCU_TEMP_H_
#define MCU_TEMP_H_

void configure_adc(void);
float get_mcu_temperature(void);
//void load_calibration_data(void);
void print_temp(float temp);


#endif /* MCU_TEMP_H_ */