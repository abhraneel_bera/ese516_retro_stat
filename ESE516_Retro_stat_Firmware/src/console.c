/*
 * console.c
 *
 * Created: 08-04-2018 11:52:44 PM
 *  Author: abhra
 */ 
#include "asf.h"
#include "console.h"
#include "socket/include/socket.h"
#include "driver/include/m2m_wifi.h"
#include "driver/source/nmasic.h"
#include "flash.h"
#include "sensors.h"
#include "gpio.h"
#include "servo.h"
#include "i2c.h"
#include "wifi.h"

/* Max size of UART buffer. */
#define UART_BUFFER_SIZE 64

/** UART module for debug. */
static struct usart_module cdc_uart_module;

/** UART buffer. */
static char uart_buffer[UART_BUFFER_SIZE];

/** Written size of UART buffer. */
static int uart_buffer_written = 0;

/** A buffer of character from the serial. */
static uint16_t uart_ch_buffer;

extern image1_meta_t image1_metadata;
extern uint8_t pu8IPAddress[];

/**
 * \brief Callback of USART input.
 *
 * \param[in] module USART module structure.
 */
static void uart_callback(const struct usart_module *const module)
{
	/* If input string is bigger than buffer size limit, ignore the excess part. */
	if (uart_buffer_written < UART_BUFFER_SIZE) {
		uart_buffer[uart_buffer_written++] = uart_ch_buffer & 0xFF;
	}
	printf("%c", uart_ch_buffer);
}

/**
 * \brief Configure UART console.
 */
void configure_console(void)
{
	struct usart_config usart_conf;

	usart_get_config_defaults(&usart_conf);
	usart_conf.mux_setting = EDBG_CDC_SERCOM_MUX_SETTING;
	usart_conf.pinmux_pad0 = EDBG_CDC_SERCOM_PINMUX_PAD0;
	usart_conf.pinmux_pad1 = EDBG_CDC_SERCOM_PINMUX_PAD1;
	usart_conf.pinmux_pad2 = EDBG_CDC_SERCOM_PINMUX_PAD2;
	usart_conf.pinmux_pad3 = EDBG_CDC_SERCOM_PINMUX_PAD3;
	usart_conf.baudrate    = 115200;

	stdio_serial_init(&cdc_uart_module, EDBG_CDC_MODULE, &usart_conf);
	/* Register USART callback for receiving user input. */
	usart_register_callback(&cdc_uart_module, (usart_callback_t)uart_callback, USART_CALLBACK_BUFFER_RECEIVED);
	usart_enable(&cdc_uart_module);
	usart_enable_callback(&cdc_uart_module, USART_CALLBACK_BUFFER_RECEIVED);
}

static void help(void){
	printf("\r\nboot loader firmware version ~ ver_bl\n\r");
	printf("Application code firmware version ~ ver_app\n\r");
	//printf("GPIO set ~ gpio_set[port][pin number]\n\r");
	//printf("GPIO clear ~ gpio_clear[port][pin number]\n\r");
	//printf("GPIO get ~ gpio_get[port][pin number]\n\r");
	printf("MAC address ~ mac\n\r");
	printf("Device IP address ~ ip\n\r");
	printf("Over the air firmware update ~ otafu\n\r");
	printf("Scan I2C devices ~ i2c_scan");
	//printf("Read sensor ~ read_sensor");
}

static void parse_and_execute(char *command){
	char help_command[] = "help";
	char firmware_version[] = "ver_bl";
	char app_firmware_version[] = "ver_app";
	//char gpio_input[] ="gpio_set";
	//char gpio_clear[]="gpio_clear";
	//char gpio_get[]="gpio_get";
	char mac_id[] = "mac";
	char ip[]="ip";
	char otafu[]="otafu";
	char i2c_scan[]="i2c_scan";
	//char read_sensor[]="read_sensor";
	
	if(!strcmp(command, help_command)){
		help();
	}
	else if(!strcmp(command, firmware_version)){
		printf("Firmware image version: %lu", image1_metadata.version);
	}
	else if(!strcmp(command, app_firmware_version)){
		printf("Firmware image version: %lu", image1_metadata.version);
	}
	else if(!strcmp(command, mac_id)){
		uint8 pu8MacAddr[6];
		if(m2m_wifi_get_mac_address	(&pu8MacAddr[0]) == M2M_SUCCESS ){
			printf("MAC Address: %x:%x:%x:%x:%x:%x", pu8MacAddr[0], pu8MacAddr[1], pu8MacAddr[2], pu8MacAddr[3], pu8MacAddr[4], pu8MacAddr[5]);
		}
	}
	else if(!strcmp(command, otafu)){
		check_and_update_firmware();
	}
	else if(!strcmp(command, ip)){
		printf("IP address is %u.%u.%u.%u", pu8IPAddress[0], pu8IPAddress[1], pu8IPAddress[2], pu8IPAddress[3]);
	}
	else if(!strcmp(command, i2c_scan)){
		scan_i2c();
	}
	else{
		printf("Invalid input !!!");
	}
}

/**
 * \brief Checking the USART buffer.
 *
 * Finding the new line character(\n or \r\n) in the USART buffer.
 */
void check_usart_buffer(void)
{
	int i;
	char command[UART_BUFFER_SIZE];
	if (uart_buffer_written >= UART_BUFFER_SIZE) {
		help();
		uart_buffer_written = 0;
		printf("\r\n>>");
		return;
	}
	for(i = 0; i < uart_buffer_written; i++){
		if (uart_buffer[i] == '\r'){
			int bytes = i + 1;
			if(i == 0){
				printf("\r\n>>");
				uart_buffer_written = 0;
				return;
			}
			snprintf(command, bytes, uart_buffer);
			command[bytes] = '\0';
			parse_and_execute(command);
			printf("\r\n>>");
			uart_buffer_written = 0;
			return;
		}
	}
}

void poll_uart(void){
	/* Try to read user input from USART. */
	usart_read_job(&cdc_uart_module, &uart_ch_buffer);
}