/**
 * \file
 *
 * \brief Empty user application template
 *
 */

/**
 * \mainpage User Application template doxygen documentation
 *
 * \par Empty user application template
 *
 * This is a bare minimum user application template.
 *
 * For documentation of the board, go \ref group_common_boards "here" for a link
 * to the board-specific documentation.
 *
 * \par Content
 *
 * -# Include the ASF header files (through asf.h)
 * -# Minimal main function that starts with a call to system_init()
 * -# Basic usage of on-board LED and button
 * -# "Insert application code here" comment
 *
 */

/*
 * Include header files for all drivers that have been imported from
 * Atmel Software Framework (ASF).
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */
#include <asf.h>
#include "wifi.h"
#include "flash.h"
#include "mcu_temp.h"
#include "console.h"
#include "i2c.h"
#include "sensors.h"
#include "servo.h"
#include "gpio.h"

int main (void)
{
	/* Initialize the board. */
	system_init();
	configure_gpio();
	configure_nvm();
	/* Initialize the UART console. */
	configure_console();
	printf("\r\n\r\n##### Smart Retro-stat #####\r\n\r\n");
	printf("This example requires the AP to have internet access.\r\n");
	configure_adc();
	configure_i2c();
	servo_config();
	set_servo(420);
	init_wifi();
	/* Initialize Flash storage. */
	init_storage();
	
	mcp9808_init();
	ccs811_init();
	connect_wifi();
	check_and_update_firmware();
	print_temp(get_mcu_temperature());
	scan_i2c();

	while (1) {
		poll_mqtt();
		poll_uart();

		check_usart_buffer();
	}
}
