/*
 * sensors.h
 *
 * Created: 09-04-2018 12:11:41 AM
 *  Author: abhra
 */ 


#ifndef SENSORS_H_
#define SENSORS_H_

typedef enum{
	TEMPERATURE,
	SMOKE,
	PIR,
	SERVO_FEEDBACK
	}sensor_type;

void ccs811_start_app(void);
uint8_t ccs811_error_check(void);
void ccs811_init(void);
uint8_t ccs811_data_ready(void);
uint16_t ccs811_get_ec02(void);
uint16_t ccs811_get_tvoc(void);

void mcp9808_init(void);
float mcp9808_get_temperature(void);
void mcp9808_handle_irq(void);

#endif /* SENSORS_H_ */