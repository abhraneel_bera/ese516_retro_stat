/*
 * servo.h
 *
 * Created: 09-04-2018 12:26:11 AM
 *  Author: abhra
 */ 


#ifndef SERVO_H_
#define SERVO_H_

void servo_config(void);
void enable_servo(bool state);
void set_servo(uint16_t on_time);
uint16_t get_servo_position(void);
void start_led_fade(void);
void stop_led_fade(void);
void start_servo_callibration(void);
void stop_servo_callibration(void);
void do_servo_callibration(void);
void command_servo(uint16_t command, uint8_t settle_time);


#endif /* SERVO_H_ */