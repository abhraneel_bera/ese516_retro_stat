/*
 * servo.c
 *
 * Created: 09-04-2018 12:25:54 AM
 *  Author: abhra
 */ 
#include "asf.h"
#include "servo.h"
#include "gpio.h"

#define SERVO_MIN 420
#define SERVO_MAX 2300

#define POS_MIN 830
#define POS_MAX 4095

struct tcc_module tcc_instance;
struct tcc_config config_tcc;
extern struct adc_module adc_instance;

uint32_t seconds_elapsed = 0;
bool led_fade = false, up = true, servo_callibration = false, move_servo = false;
uint16_t fade_value = 0;

uint16_t servo_min = POS_MAX, servo_max = POS_MIN;

uint32_t servo_start_time = 0, servo_settle_time = 0;

// Taken from Arduino library
static long map(long x, long in_min, long in_max, long out_min, long out_max)
{
	return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

static void tcc_callback_timer(struct tcc_module *const module_inst){
	static int count;
	if(++count == 50){
		count = 0;
		seconds_elapsed++;
	}
	if(led_fade){
		if(up){
			fade_value = fade_value + 1000;
			if(fade_value == 0xEA60)
				up = false;
		}
		else{
			fade_value = fade_value - 1000;
			if(fade_value == 0)
				up = true;
		}
		tcc_set_compare_value(&tcc_instance, 1, fade_value);
	}
	if(servo_callibration)
		do_servo_callibration();
	if(move_servo){
		if((seconds_elapsed - servo_start_time) >= servo_settle_time){
			enable_servo(false);
			move_servo = false;
			tcc_set_compare_value(&tcc_instance, 3, 0);
		}
	}
	
/*	enable_servo(true);
	if(up){
		fade_value = fade_value + 10;
		if(fade_value >= SERVO_MAX)
		up = false;
	}
	else{
		fade_value = fade_value - 10;
		if(fade_value <= SERVO_MIN)
		up = true;
	}
	tcc_set_compare_value(&tcc_instance, 3, 3*fade_value);
	printf("Servo pos: %d\r\n", get_servo_position());*/
}

static void configure_tcc_callbacks(void)
{
	tcc_register_callback(&tcc_instance, tcc_callback_timer,
	TCC_CALLBACK_OVERFLOW);
	tcc_enable_callback(&tcc_instance, TCC_CALLBACK_OVERFLOW);
}

void servo_config(void){
	tcc_get_config_defaults(&config_tcc, TCC0);
	config_tcc.counter.clock_prescaler = TCC_CLOCK_PRESCALER_DIV16;
	config_tcc.counter.period = 0xEA60;
	config_tcc.compare.wave_generation = TCC_WAVE_GENERATION_SINGLE_SLOPE_PWM;
	config_tcc.compare.match[3] = (0xEA60/10);
	config_tcc.pins.enable_wave_out_pin[3] = true;
	config_tcc.pins.wave_out_pin[3]        = PIN_PA17F_TCC0_WO7;
	config_tcc.pins.wave_out_pin_mux[3]    = MUX_PA17F_TCC0_WO7;
	config_tcc.compare.match[1] = 0;
	config_tcc.pins.enable_wave_out_pin[1] = true;
	config_tcc.pins.wave_out_pin[1]        = PIN_PA05E_TCC0_WO1;
	config_tcc.pins.wave_out_pin_mux[1]    = MUX_PA05E_TCC0_WO1;
	tcc_init(&tcc_instance, TCC0, &config_tcc);
	tcc_enable(&tcc_instance);
	configure_tcc_callbacks();
}

void enable_servo(bool state){
	port_pin_set_output_level(SERVO_ENABLE, state);
}

void set_servo(uint16_t on_time){
	if(on_time <= SERVO_MIN)
		on_time = SERVO_MIN;
	if(on_time >= SERVO_MAX)
		on_time = SERVO_MAX;
	tcc_set_compare_value(&tcc_instance, 3, (3 * on_time));
}

void start_led_fade(void){
	fade_value = 0;
	up = true;
	led_fade = true;
}

void stop_led_fade(void){
	led_fade = false;
	fade_value = 0;
	up = true;
	tcc_set_compare_value(&tcc_instance, 1, fade_value);
}

uint16_t get_servo_position(void){
	uint16_t result;
	adc_set_positive_input(&adc_instance, ADC_POSITIVE_INPUT_PIN0);
	adc_start_conversion(&adc_instance);
	do {
		/* Wait for conversion to be done and read out result */
	} while (adc_read(&adc_instance, &result) == STATUS_BUSY);
	return result;
}

void do_servo_callibration(void){
	uint16_t pos = get_servo_position();
	if(pos >= servo_max)
	servo_max = pos;
	if(pos <= servo_min)
	servo_min = pos;
}

void start_servo_callibration(void){
	tcc_set_compare_value(&tcc_instance, 3, 0);
	enable_servo(true);
	start_led_fade();
	servo_min = 4096;
	servo_max = 0;
	servo_callibration = true;
}

void stop_servo_callibration(void){
	stop_led_fade();
	enable_servo(false);
	servo_callibration = false;
	if(servo_max >= POS_MAX)
	servo_max = POS_MAX;
	if(servo_min <= POS_MIN)
	servo_min = POS_MIN;
	printf("Servo min: %u max: %u\r\n", servo_min, servo_max);
}

void command_servo(uint16_t command, uint8_t settle_time){
	if(servo_callibration)
		return;
	uint16_t on_time = 0;
	if((servo_min == POS_MAX) && (servo_max == POS_MIN))
		return;
	if(command <= 65)
		command = 65;
	if(command >= 85)
		command = 85;
	on_time = map(command, 65, 85, servo_min, servo_max);
	on_time = map(on_time, POS_MIN, POS_MAX, SERVO_MIN, SERVO_MAX);
	servo_settle_time = settle_time;
	set_servo(on_time);
	enable_servo(true);
	servo_start_time = seconds_elapsed;
	move_servo = true;
}