/*
 * i2c.h
 *
 * Created: 09-04-2018 12:04:53 AM
 *  Author: abhra
 */ 


#ifndef I2C_H_
#define I2C_H_

void configure_i2c(void);
void scan_i2c(void);
enum status_code i2c_read_reg(uint8_t addr, uint8_t reg, uint8_t bytes);
enum status_code i2c_write_reg(uint8_t addr, uint8_t reg, uint8_t bytes);

#endif /* I2C_H_ */