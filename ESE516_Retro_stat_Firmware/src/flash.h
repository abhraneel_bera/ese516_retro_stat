/*
 * flash.h
 *
 * Created: 03-04-2018 07:30:05 PM
 *  Author: abhra
 */ 


#ifndef FLASH_H_
#define FLASH_H_

void init_storage(void);
void start_write(void);
void finish_write(void);
void save_data_to_flash(char *data, uint32_t length);
void update_image_status(void);
void set_bootloader_flag(void);
void configure_nvm(void);

typedef struct nvm_status_header{
	uint8_t no_bootloader;
	uint8_t nvm_invalid;
} nvm_status_t;

typedef struct flash_status_header{
	uint8_t image1_invalid;
	uint8_t image2_invalid;
} flash_status_t;

typedef struct image1_meta{
	uint32_t version;
	uint32_t size;
	uint32_t checksum;
} image1_meta_t;

typedef struct image2_meta{
	uint32_t version;
	uint32_t size;
	uint32_t checksum;
} image2_meta_t;

typedef enum{
	IMAGE1,
	IMAGE2
}image_t;

typedef enum{
	IMAGE1_VALID,
	IMAGE1_INVALID,
	IMAGE2_VALID,
	IMAGE2_INVALID
}image_valid_t;

void update_metadata(image_t image);
uint8_t verify_checksum(image_t image);
void write_image_state(image_valid_t validity);

#endif /* FLASH_H_ */