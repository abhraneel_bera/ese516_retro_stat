/*
 * console.h
 *
 * Created: 08-04-2018 11:53:14 PM
 *  Author: abhra
 */ 


#ifndef CONSOLE_H_
#define CONSOLE_H_

void configure_console(void);
void check_usart_buffer(void);
void poll_uart(void);

#endif /* CONSOLE_H_ */