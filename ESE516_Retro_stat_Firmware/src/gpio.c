/*
 * gpio.c
 *
 * Created: 09-04-2018 12:41:30 AM
 *  Author: abhra
 */ 
#include "asf.h"
#include "gpio.h"
#include "sensors.h"
#include "servo.h"
#include "wifi.h"

bool calibration_mode = true;

/** Button1 callback
 */
static void button1_callback(void)
{
	bool pin_state = port_pin_get_input_level(BUTTON1);
	if (pin_state) {
		//port_pin_set_output_level(LED1, false);
		} else {
			if(calibration_mode){
				start_servo_callibration();
				calibration_mode = false;
			}
			else{
				calibration_mode = true;
				stop_servo_callibration();
			}
		//port_pin_set_output_level(LED1, true);
	}
}

/** Button2 callback
 */
static void button2_callback(void)
{
	// Do nothing for now
	bool pin_state = port_pin_get_input_level(BUTTON2);
	if (pin_state) {
		port_pin_set_output_level(LED2, false);
		} else {
		port_pin_set_output_level(LED2, true);
	}
}

/** MCP callback
 */
static void mcp_callback(void)
{
		bool pin_state = port_pin_get_input_level(MCP_IRQ);
		if (!pin_state) {
			mcp9808_handle_irq();
			printf("MCP: Got IRQ\r\n");
			uint8_t temp = mcp9808_get_temperature();
			printf("Current Temp: %d\r\n", temp);
		}
		else{
			printf("MCP: Cleared IRQ\r\n");
		}
}

/** CCS callback
 */
static void ccs_callback(void)
{
	bool pin_state = port_pin_get_input_level(CCS_IRQ);
	if (pin_state) {
		} else {
			if(ccs811_data_ready() && !ccs811_error_check()){
				int eco2 = ccs811_get_ec02();
				printf("Calculated eCO2: %d ppm\r\n", eco2);
				if(eco2 > 1500)
					publish_message(SMOKE, 1);
			}
	}
}

/** PIR callback
 */
static void pir_callback(void)
{
	bool pin_state = port_pin_get_input_level(PIR_IRQ);
	if (!pin_state) {
		publish_message(PIR, 0);
		printf("PIR: unoccupied\r\n");
		//port_pin_set_output_level(LED2, false);
		} else {
		publish_message(PIR, 1);
		printf("PIR: occupied\r\n");
		//port_pin_set_output_level(LED2, true);
	}
}

/** Configures the External Interrupt Controller to detect changes in the board
 *  button state.
 */
static void configure_extint(void)
{
	struct extint_chan_conf eint_chan_conf;
	extint_chan_get_config_defaults(&eint_chan_conf);

	eint_chan_conf.gpio_pin           = BUTTON1_EIC_PIN;
	eint_chan_conf.gpio_pin_mux       = BUTTON1_EIC_MUX;
	eint_chan_conf.detection_criteria = EXTINT_DETECT_BOTH;
	eint_chan_conf.filter_input_signal = true;
	extint_chan_set_config(BUTTON1_EIC_NUM, &eint_chan_conf);
	eint_chan_conf.gpio_pin           = BUTTON2_EIC_PIN;
	eint_chan_conf.gpio_pin_mux       = BUTTON2_EIC_MUX;
	eint_chan_conf.detection_criteria = EXTINT_DETECT_BOTH;
	eint_chan_conf.filter_input_signal = true;
	extint_chan_set_config(BUTTON2_EIC_NUM, &eint_chan_conf);
	eint_chan_conf.gpio_pin           = MCP_IRQ_EIC_PIN;
	eint_chan_conf.gpio_pin_mux       = MCP_IRQ_EIC_MUX;
	eint_chan_conf.detection_criteria = EXTINT_DETECT_BOTH;
	eint_chan_conf.filter_input_signal = true;
	extint_chan_set_config(MCP_IRQ_EIC_NUM, &eint_chan_conf);
	eint_chan_conf.gpio_pin           = CCS_IRQ_EIC_PIN;
	eint_chan_conf.gpio_pin_mux       = CCS_IRQ_EIC_MUX;
	eint_chan_conf.detection_criteria = EXTINT_DETECT_BOTH;
	eint_chan_conf.filter_input_signal = true;
	extint_chan_set_config(CCS_IRQ_EIC_NUM, &eint_chan_conf);
	eint_chan_conf.gpio_pin           = PIR_IRQ_EIC_PIN;
	eint_chan_conf.gpio_pin_mux       = PIR_IRQ_EIC_MUX;
	eint_chan_conf.detection_criteria = EXTINT_DETECT_BOTH;
	eint_chan_conf.filter_input_signal = true;
	extint_chan_set_config(PIR_IRQ_EIC_NUM, &eint_chan_conf);
}

/** Configures and registers the External Interrupt callback function with the
 *  driver.
 */
static void configure_eic_callback(void)
{
	extint_register_callback(button1_callback, BUTTON1_EIC_NUM, EXTINT_CALLBACK_TYPE_DETECT);
	extint_chan_enable_callback(BUTTON1_EIC_NUM, EXTINT_CALLBACK_TYPE_DETECT);
	extint_register_callback(button2_callback, BUTTON2_EIC_NUM, EXTINT_CALLBACK_TYPE_DETECT);
	extint_chan_enable_callback(BUTTON2_EIC_NUM, EXTINT_CALLBACK_TYPE_DETECT);
	extint_register_callback(mcp_callback, MCP_IRQ_EIC_NUM, EXTINT_CALLBACK_TYPE_DETECT);
	extint_chan_enable_callback(MCP_IRQ_EIC_NUM, EXTINT_CALLBACK_TYPE_DETECT);
	extint_register_callback(ccs_callback, CCS_IRQ_EIC_NUM, EXTINT_CALLBACK_TYPE_DETECT);
	extint_chan_enable_callback(CCS_IRQ_EIC_NUM, EXTINT_CALLBACK_TYPE_DETECT);
	extint_register_callback(pir_callback, PIR_IRQ_EIC_NUM, EXTINT_CALLBACK_TYPE_DETECT);
	extint_chan_enable_callback(PIR_IRQ_EIC_NUM, EXTINT_CALLBACK_TYPE_DETECT);
}

/** Configures gpio
 */
void configure_gpio(void){
	struct port_config pin_conf;
	port_get_config_defaults(&pin_conf);

	/* Configure LEDs and Servo enable as outputs, turn them off */
	pin_conf.direction  = PORT_PIN_DIR_OUTPUT;
	port_pin_set_config(LED1, &pin_conf);
	port_pin_set_output_level(LED1, false);
	port_pin_set_config(LED2, &pin_conf);
	port_pin_set_output_level(LED2, false);
	port_pin_set_config(SERVO_ENABLE, &pin_conf);
	port_pin_set_output_level(SERVO_ENABLE, false);
	port_pin_set_config(SERVO_PWM, &pin_conf);
	port_pin_set_output_level(SERVO_PWM, false);

	/* Set buttons and IRQs as inputs */
	pin_conf.direction  = PORT_PIN_DIR_INPUT;
	pin_conf.input_pull = PORT_PIN_PULL_UP;
	port_pin_set_config(BUTTON1, &pin_conf);
	port_pin_set_config(BUTTON2, &pin_conf);
	port_pin_set_config(MCP_IRQ, &pin_conf);
	port_pin_set_config(CCS_IRQ, &pin_conf);
	//pin_conf.input_pull = PORT_PIN_PULL_DOWN;
	port_pin_set_config(PIR_IRQ, &pin_conf);
	
	configure_extint();
	configure_eic_callback();
}