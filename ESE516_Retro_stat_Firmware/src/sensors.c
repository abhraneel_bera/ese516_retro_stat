/*
 * sensors.c
 *
 * Created: 09-04-2018 12:11:23 AM
 *  Author: abhra
 */ 
#include "asf.h"
#include "sensors.h"
#include "i2c.h"
#include "iot/mqtt/mqtt.h"
#include "wifi.h"
#include <math.h>

#define CCS811_ADDRESS				0x5A
#define CCS811_ID					0x81

#define CCS811_STATUS				0x00
#define CCS811_MEAS_MODE			0x01
#define CCS_811_ALG_RESULT_DATA		0x02
#define CCS811_THRESHOLD			0x10
#define CCS811_HW_ID				0x20
#define CCS811_ERROR_ID				0xE0
#define CCS811_START_APP			0xF4

#define MCP9808_ADDRESS				0x18
#define MCP9808_MANUFAFTURE_ID		0x0054
#define MCP9808_DEVICE_ID			0x04

#define MCP9808_CONFIG				0b1
#define MCP9808_TEMP_UP_BOUND		0b10
#define MCP9808_TEMP_LOW_BOUND		0b11
#define MCP9808_CRITICAL_TEMP		0b100
#define MCP9808_TEMP_REG			0b101
#define MCP9808_MANUF_ID			0b110
#define MCP9808_DEV_ID				0b111
#define MCP9808_RESOLUTION			0b1000

extern uint8_t wr_buffer[];
extern uint8_t rd_buffer[];

extern struct i2c_master_module i2c_master_instance;

extern struct i2c_master_packet wr_packet;

extern struct i2c_master_packet rd_packet;

volatile int current_temperature = 0;

void ccs811_start_app(void){
	//enum status_code i2c_status;
	wr_packet.address     = CCS811_ADDRESS;
	wr_packet.data_length = 1;
	wr_buffer[0]          = CCS811_START_APP;
	wr_packet.data        = wr_buffer;
	i2c_master_write_packet_wait(&i2c_master_instance, &wr_packet);
}

uint8_t ccs811_error_check(void){
	i2c_read_reg(CCS811_ADDRESS, CCS811_STATUS, 1);
	if(rd_buffer[0] & (1 << 0)){
		i2c_read_reg(CCS811_ADDRESS, CCS811_ERROR_ID, 1);
		printf("Error %d\r\n", rd_buffer[0]);
		return 1;
	}
	return 0;
}

void ccs811_init(void){
	i2c_read_reg(CCS811_ADDRESS, CCS811_HW_ID, 1);
	if(rd_buffer[0] != CCS811_ID){
		printf("CCS811 not found!!\r\n");
		return;
	}
	i2c_read_reg(CCS811_ADDRESS, CCS811_STATUS, 1);
	if(!(rd_buffer[0] & (1 << 7))){
		ccs811_start_app();
		if(ccs811_error_check()){
			printf("CCS811 init failed!!\r\n");
			return;
		}
	}
	i2c_read_reg(CCS811_ADDRESS, CCS811_MEAS_MODE, 1);
	wr_buffer[1] = (1 << 4) | (1 << 3) | (1 << 2);
	i2c_write_reg(CCS811_ADDRESS, CCS811_MEAS_MODE, 1);
	wr_buffer[1] = 0x05;
	wr_buffer[2] = 0xDC;
	wr_buffer[3] = 0x09;
	wr_buffer[4] = 0xC4;
	i2c_write_reg(CCS811_ADDRESS, CCS811_THRESHOLD, 4);
	if(ccs811_data_ready() && !ccs811_error_check()){
		printf("Calculated eCO2: %d ppm\r\n", ccs811_get_ec02());
	}
	ccs811_error_check();
}

uint8_t ccs811_data_ready(void){
	i2c_read_reg(CCS811_ADDRESS, CCS811_STATUS, 1);
	if(rd_buffer[0] & (1 << 3)){
		return 1;
	}
	else{
		return 0;
	}
}

uint16_t ccs811_get_ec02(void){
	i2c_read_reg(CCS811_ADDRESS, CCS_811_ALG_RESULT_DATA, 4);
	return ((rd_buffer[0] << 8) | (rd_buffer[1]));
}

uint16_t ccs811_get_tvoc(void){
	i2c_read_reg(CCS811_ADDRESS, CCS_811_ALG_RESULT_DATA, 4);
	return ((rd_buffer[2] << 8) | (rd_buffer[3]));
}


void mcp9808_init(void){
	uint16_t manuf_id, dev_id;
	i2c_read_reg(MCP9808_ADDRESS, MCP9808_MANUF_ID, 2);
	manuf_id = (rd_buffer[0] << 8) | (rd_buffer[1]);
	i2c_read_reg(MCP9808_ADDRESS, MCP9808_DEV_ID, 2);
	dev_id = rd_buffer[0];
	if((dev_id != MCP9808_DEVICE_ID) && (manuf_id != MCP9808_MANUFAFTURE_ID)){
		printf("MCP9808 not found\r\n");
		return;
	}
	uint16_t config = 0;
	wr_buffer[1] = 0;//(config >> 8) & 0xFF;
	wr_buffer[2] = 0;//config & 0xFF;
	i2c_write_reg(MCP9808_ADDRESS, MCP9808_CONFIG, 2);

	float temp = mcp9808_get_temperature();
	uint8_t floor_temp = floor(temp);
	uint8_t ceil_temp = ceil(temp);
	wr_buffer[1] = (ceil_temp >> 4);
	wr_buffer[2] = (ceil_temp << 4);
	i2c_write_reg(MCP9808_ADDRESS, MCP9808_TEMP_UP_BOUND, 2);
	wr_buffer[1] = (floor_temp >> 4);
	wr_buffer[2] = (floor_temp << 4);
	i2c_write_reg(MCP9808_ADDRESS, MCP9808_TEMP_LOW_BOUND, 2);
	wr_buffer[1] = (uint8_t)((0xFF) >> 4);
	wr_buffer[2] = (uint8_t)((0xFF) << 4);
	i2c_write_reg(MCP9808_ADDRESS, MCP9808_CRITICAL_TEMP, 2);
	
	config = (1 << 3) | (1 << 0); // Enable alert, Set alert to interrupt mode
	wr_buffer[1] = (config >> 8) & 0xFF;
	wr_buffer[2] = config & 0xFF;
	i2c_write_reg(MCP9808_ADDRESS, MCP9808_CONFIG, 2);
}

float mcp9808_get_temperature(void){
	float temp;
	i2c_read_reg(MCP9808_ADDRESS, MCP9808_TEMP_REG, 2);
	if(rd_buffer[0] & (1 << 4))
		temp = 256 - (float)((rd_buffer[0] & 0xF) * 16 + ((float)rd_buffer[1] / (float)16));
	else
		temp = (float)((rd_buffer[0] & 0xF) * 16 + ((float)rd_buffer[1] / (float)16));
	return temp;
}

void mcp9808_handle_irq(void){
	
	uint16_t config = 0;
	
	float temp = mcp9808_get_temperature();
	current_temperature = temp;
	uint8_t floor_temp = floor(temp);
	uint8_t ceil_temp = ceil(temp);
	wr_buffer[1] = (uint8_t)((0xFF) >> 4);
	wr_buffer[2] = (uint8_t)((0xFF) << 4);
	i2c_write_reg(MCP9808_ADDRESS, MCP9808_CRITICAL_TEMP, 2);
	wr_buffer[1] = (ceil_temp >> 4);
	wr_buffer[2] = (ceil_temp << 4);
	i2c_write_reg(MCP9808_ADDRESS, MCP9808_TEMP_UP_BOUND, 2);
	wr_buffer[1] = (floor_temp >> 4);
	wr_buffer[2] = (floor_temp << 4);
	i2c_write_reg(MCP9808_ADDRESS, MCP9808_TEMP_LOW_BOUND, 2);
	
	i2c_read_reg(MCP9808_ADDRESS, MCP9808_CONFIG, 2);
	config = (rd_buffer[0] << 8) | (rd_buffer[1]);
	
	config |= (1 << 5); // Clear interrupt
	wr_buffer[1] = (config >> 8) & 0xFF;
	wr_buffer[2] = config & 0xFF;
	i2c_write_reg(MCP9808_ADDRESS, MCP9808_CONFIG, 2);
	
	uint8_t tmp = ((temp * 9) / 5) + 32;
	publish_message(TEMPERATURE, tmp);
}