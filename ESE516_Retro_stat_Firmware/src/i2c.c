/*
 * i2c.c
 *
 * Created: 09-04-2018 12:04:30 AM
 *  Author: abhra
 */ 
#include "asf.h"
#include "i2c.h"

#define DATA_LENGTH 8
uint8_t wr_buffer[DATA_LENGTH];
uint8_t rd_buffer[DATA_LENGTH];

struct i2c_master_module i2c_master_instance;

struct i2c_master_packet wr_packet = {
	//.address          = SLAVE_ADDRESS,
	.data_length      = DATA_LENGTH,
	.data             = wr_buffer,
	.ten_bit_address  = false,
	.high_speed       = false,
	.hs_master_code   = 0x00,
};

struct i2c_master_packet rd_packet = {
	//.address          = SLAVE_ADDRESS,
	.data_length      = DATA_LENGTH,
	.data             = rd_buffer,
	.ten_bit_address  = false,
	.high_speed       = false,
	.hs_master_code   = 0x00,
};


void configure_i2c(void)
{
	struct i2c_master_config config_i2c_master;
	i2c_master_get_config_defaults(&config_i2c_master);
	config_i2c_master.pinmux_pad0       = PINMUX_PA08C_SERCOM0_PAD0;
	config_i2c_master.pinmux_pad1       = PINMUX_PA09C_SERCOM0_PAD1;
	while(i2c_master_init(&i2c_master_instance, SERCOM0, &config_i2c_master) != STATUS_OK);
	i2c_master_enable(&i2c_master_instance);
}

void scan_i2c(void){
	uint8_t addr;
	enum status_code i2c_status;
	for(addr = 0; addr < 0xFF>>1; addr++){
		wr_packet.address     = addr;
		wr_packet.data_length = 0;
		wr_buffer[0]          = 0x00;
		wr_packet.data        = wr_buffer;
		i2c_status = i2c_master_write_packet_wait(&i2c_master_instance, &wr_packet);
		if(i2c_status == STATUS_OK){
			printf("I2C device found at 0x%x\r\n", addr);
		}
		else if(i2c_status == STATUS_ERR_BAD_ADDRESS){
			continue;
		}
		else{
			printf("Unknown error\r\n");
		}
	}
}

enum status_code i2c_read_reg(uint8_t addr, uint8_t reg, uint8_t bytes){
	enum status_code i2c_status;
	wr_packet.address     = addr;
	wr_packet.data_length = 1;
	wr_buffer[0]          = reg;
	wr_packet.data        = wr_buffer;
	rd_packet.address     = addr;
	rd_packet.data_length = bytes;
	rd_packet.data        = rd_buffer;
	i2c_status = i2c_master_write_packet_wait_no_stop(&i2c_master_instance, &wr_packet);
	if( i2c_status == STATUS_OK ){
		i2c_status = i2c_master_read_packet_wait_no_stop(&i2c_master_instance, &rd_packet);
	}
	else if(i2c_status == STATUS_ERR_BAD_ADDRESS){
		printf("Device not found at 0x%x\r\n", wr_packet.address);
	}
	i2c_master_send_stop(&i2c_master_instance);
	return i2c_status;
}

enum status_code i2c_write_reg(uint8_t addr, uint8_t reg, uint8_t bytes){
	enum status_code i2c_status;
	wr_packet.address     = addr;
	wr_packet.data_length = bytes + 1;
	wr_buffer[0]          = reg;
	wr_packet.data        = wr_buffer;
	i2c_status = i2c_master_write_packet_wait(&i2c_master_instance, &wr_packet);
	if(i2c_status == STATUS_ERR_BAD_ADDRESS){
		printf("Device not found at 0x%x\r\n", addr);
	}
	return i2c_status;
}